// setTimeout(() => {
//     $('#MMW_loading').fadeOut(400);
// }, 300);

let url_play_game = "https://play.mymasterwar.com";

window.addEventListener("scroll", handleToggleScrollToTop);

function handlePlayNow() {
    window.open(`${url_play_game}${window.location.search}`, "_blank");
}

function handleGotoSmartContract() {
    window.open(`https://bscscan.com/address/0xf3147987a00d35eecc10c731269003ca093740ca`, "_blank");
}

function handleToggleScrollToTop(e) {
    const windowx = e.currentTarget;
    if (windowx.scrollY > 200) {
        $('#to_top').fadeIn(300);
    } else {
        $('#to_top').fadeOut(300);
    }
    if ((windowx.innerHeight + windowx.scrollY) >= document.body.offsetHeight) {
        $("#social-iconbar").fadeOut(300);
    } else {
        $("#social-iconbar").fadeIn(300);
    }
}

function playVideo(event, url) {
    event.preventDefault();
    event.stopPropagation();
    $("#IFRAME_VIDEO").attr('src', url);
    var myModalEl = document.getElementById('videoModal')
    var videoModal = new bootstrap.Modal(myModalEl, {});
    videoModal.show();
    myModalEl.addEventListener('hidden.bs.modal', function(event) {
        $("#IFRAME_VIDEO").attr('src', '');
    });
}
var TEAMS = [{
        "fullName": "Dominic Nguyen",
        "jobTitle": "Co-Founder/ CEO",
        "avatar": "/images/teams/t1.png",
        "linkedin": "https://www.linkedin.com/in/dominicnguyennn/",
        "telegram": "https://t.me/biettruoctuonglaii",
        "mail": "dominic@mymasterwar.com",
        "description": "<p>• Understanding of Vietnam market, valuable connections and people, strong leadership; understanding of strategic and processes, good organization & interpersonal skills;</p><p>• Good knowledge and experience in design and architecture of large-scale, big data software systems. Have experience with software development for a real time operating environment, big user system, and social network;</p><p>• 7 years’ experience as hands on CEO (Chief Executive Officer);</p><p>• 7 years’ experience as hands on CTO (Chief Technology Officer);</p><p>• 3 years’ experience as hands-on experience in online game, data science, Big Data, Artificial Intelligence, Machine Learning;</p><p>• 10 years’ experience as hands on project management and global delivery manager in Japan market.</p>"
    },
    {
        "fullName": "Henry Nguyen",
        "jobTitle": "Co-Founder/CTO",
        "avatar": "/images/teams/t5.png",
        "linkedin": "https://www.linkedin.com/in/henry-nguyen-0512a652/",
        "mail": "",
        "description": "<p>Henry is a person with extensive experience in the field of security systems and blockchain. He is the Chief Technology Officer of Mymasterwar.</p><p>Working as a blockchain architect and engineer on Krispay, a digital wallet that turns Singair's KrisFlyer mileage into an instant purchase on Krispay for Singapore Airlines. </p><p>Working on Euphoreum, a crypto wallet integrated into email makes it easier to send/receive loyalty rewards via email (https://euphoreum.com/).</p>"
    },
    {
        "fullName": "HA VU, Ph.D. in AI",
        "jobTitle": "SENIOR ADVISOR",
        "avatar": "/images/teams/t8.png",
        "linkedin": "",
        "mail": "",
        "description": "<p>HA VU, Ph.D. in AI, has more than 20 years of experience in digital strategy, innovation, and AI, with several prestigious international awards for jobs ranging from engineering to research. He has spent most of their time in the fields such as gaming & media, telecom, insurance, and finance. Though in these fields, his research & development on new market opportunities cover a broader range of industrial sectors, leaning on ecosystem development.</p>"
    },
    {
        "fullName": "Quan Tran",
        "jobTitle": "CTO Blockchain",
        "avatar": "/images/teams/t2.png",
        "linkedin": "https://www.linkedin.com/in/quanttb/",
        "mail": "",
        "description": "<p>Lead the engineering team and be responsible for technical strategy and engineering operations.</p><p>Master of Computer Science, HCMC University of Technology, extensive experience in technical development and blockchain technology, NFT.</p><p>BTA-certified blockchain developer with a focus on Hyperledger Fabric and Ethereum</p>"
    },
    {
        "fullName": "Vo Nguyen",
        "jobTitle": "Chief Gaming Officer",
        "avatar": "/images/teams/t3.png",
        "linkedin": "https://www.linkedin.com/in/vo-nguyen-5b417021a/",
        "mail": "",
        "description": "<p>Leader team Building and developing an online game server system in the Philippine market</p><p>Used to develop a system of online folk games such as chess, face-down chess, checkers, and portals including 20 online folk games.</p><p>Game products have reached the top 1 trending on google play</p>"
    },
    {
        "fullName": "Thanh Nguyen",
        "jobTitle": "Product Lead",
        "avatar": "/images/teams/t4.png",
        "linkedin": "https://www.linkedin.com/in/vietthanh-dot-info/",
        "mail": "",
        "description": "<p>Project leader of mobile game products under BP Casual -soha game Vccoprt</p><p>Head of construction, sales of VAS services on games of TT media Viettel telecom</p><p>Scenario design manager for Game projects under Inet Corporation</p>"
    },
    {
        "fullName": "Thoai Nguyen",
        "jobTitle": "Lead the engineering team",
        "avatar": "/images/teams/t7.png",
        "linkedin": "https://www.linkedin.com/in/thoai-nguyen-b3a94221a",
        "mail": "",
        "description": "<p>Lead the engineering team and be responsible for technical strategy and engineering operations.</p><p>Extensive experience in technical development and blockchain technology, NFT.</p><p>Codar developer, IBM Blockchain Foundation Developer,  and Microsoft Certified Solutions Developer Certification</p>"
    },
    {
        "fullName": "Jsmile Bui",
        "jobTitle": "Lead the Frontend teams",
        "avatar": "/images/teams/t6.png",
        "linkedin": "https://www.linkedin.com/in/mr-jsmile-129413193/",
        "mail": "",
        "description": "<p>Intensive experience in Frontend development (Mobile & Web)</p><p>5+ years of experience in software development </p><p>“Very Good” degree classification of IT Engineer, Hanoi University of Science and Technology</p><p>“Nhan tai dat Viet” incentive award</p>"
    }
];

function viewProfile(event, index) {
    event.preventDefault();
    event.stopPropagation();
    $('#TEAM_MODAL_TITLE').html(TEAMS[index].fullName);
    $("#TEAM_MODAL_IMAGE").attr('src', TEAMS[index].avatar);
    $('#TEAM_MODAL_LINKEDIN').hide();
    $('#TEAM_MODAL_TELEGRAM').hide();
    $('#TEAM_MODAL_MAIL').hide();
    if (TEAMS[index].linkedin) {
        $('#TEAM_MODAL_LINKEDIN').attr('href', TEAMS[index].linkedin);
        $('#TEAM_MODAL_LINKEDIN').show();
    }
    if (TEAMS[index].telegram) {
        $('#TEAM_MODAL_TELEGRAM').attr('href', TEAMS[index].telegram);
        $('#TEAM_MODAL_TELEGRAM').show();
    }
    if (TEAMS[index].mail) {
        $('#TEAM_MODAL_MAIL').attr('href', `mailto:${TEAMS[index].mail}`);
        $('#TEAM_MODAL_MAIL').show();
    }
    $('#TEAM_MODAL_NAME').html(TEAMS[index].fullName);
    $('#TEAM_MODAL_POSITION').html(TEAMS[index].jobTitle);
    $('#TEAM_MODAL_CONTENT').html(TEAMS[index].description);

    var myModalEl = document.getElementById('teamModal')
    var teamModal = new bootstrap.Modal(myModalEl, {});
    teamModal.show();
    myModalEl.addEventListener('hidden.bs.modal', function(event) {
        $('#TEAM_MODAL_TITLE').html("");
        $("#TEAM_MODAL_IMAGE").attr('src', "");
        $('#TEAM_MODAL_LINKEDIN').hide();
        $('#TEAM_MODAL_LINKEDIN').attr('href', "");
        $('#TEAM_MODAL_TELEGRAM').hide();
        $('#TEAM_MODAL_TELEGRAM').attr('href', "");
        $('#TEAM_MODAL_MAIL').hide();
        $('#TEAM_MODAL_MAIL').attr('href', "");
        $('#TEAM_MODAL_NAME').html("");
        $('#TEAM_MODAL_POSITION').html("");
        $('#TEAM_MODAL_CONTENT').html("");
    });
}

var FEATURES = [{
        "name": "Heroes",
        "image": "/images/features/chientuong/chientuong1.png?v=1",
        "description": "To become a courageous and unrivaled hero, you must go through hundreds of heaven challenges, pass thousands of life-and-death missions to train all your abilities.",
        "albums": [
            "/images/features/chientuong/chientuong1.png?v=1",
            "/images/features/chientuong/chientuong2.png?v=1"
        ],
        "video": "https://www.youtube.com/embed/Qr2YuGEDgT0"
    },
    {
        "name": "World Boss",
        "image": "/images/features/bossTG/bossTG1.png?v=1",
        "description": "Heaven monsters are summoned to harm the world, so 12 vassals have to all gather to defeat them and earn their resources. Those monsters contain a lot of valuable resources, destroying them can earn many incredible rewards.",
        "albums": [
            "/images/features/bossTG/bossTG2.png?v=1",
            // "/images/features/bossTG/bossTG1.png",
            "/images/features/bossTG/bossTG3.png?v=1"
        ],
        "video": "https://www.youtube.com/embed/y--BTu0-qNQ"
    },
    {
        "name": "Strategy",
        "image": "/images/features/tranphap/tranphap1.png?v=1",
        "description": "Stimulating all kinds of strategies can bring players the power to destroy heaven and earth since ancient times. The strategies are modified to match the types of troops to increase the maximum strength of heroes. For that reason, players can use their strategies to improve the power of soldiers.",
        "albums": [
            "/images/features/tranphap/tranphap1.png?v=1",
            "/images/features/tranphap/tranphap2.png?v=1",
            "/images/features/tranphap/tranphap3.png?v=1"
        ],
        "video": "https://www.youtube.com/embed/kTnfWOtFvsY"
    },
    {
        "name": "Tavern",
        "image": "/images/features/anhhungtuulau/anhhungtuulau1.png?v=1",
        "description": "The Tavern is where all talented heroes gather to talk about their stories. Heroes are just ordinary people and will become ashes over time if they don't choose the right master. Who will be named in the history books, who will be forgotten in the history line, it all depends on your choice.",
        "albums": [
            "/images/features/anhhungtuulau/anhhungtuulau1.png?v=1",
            "/images/features/anhhungtuulau/anhhungtuulau2.png?v=1"
        ],
        "video": "https://www.youtube.com/embed/yzyScodnTxg"
    },
    {
        "name": "Nation War",
        "image": "/images/features/quocchien/quocchien1.png?v=1",
        "description": "The dispute between the three forces has been going on for hundreds of years and destroying thousands of people's lives. In that scenario, who will be the son of heaven to unify the country and bring peace to people.",
        "albums": [
            "/images/features/quocchien/quocchien1.png?v=1",
            "/images/features/quocchien/quocchien2.png?v=1"
        ],
        "video": "https://www.youtube.com/embed/nBn-dZc7jOg"
    },
    {
        "name": "Resources",
        "image": "/images/features/tainguyen/tainguyen1.png?v=1",
        "description": "Minerals lie under thousands of meters from the ground, which is extremely difficult to discover and exploit. Each mineral mine produces a large number of resources leading to a lot of fierce disputes between forces.",
        "albums": [
            "/images/features/tainguyen/tainguyen1.png?v=1",
            "/images/features/tainguyen/tainguyen2.png?v=1"
        ],
        "video": "https://www.youtube.com/embed/dBVwfduUgKQ"
    }
]

function viewFeature(event, index) {
    event.preventDefault();
    event.stopPropagation();
    var indicators = '';
    var images = '';
    for (let i = 0; i < FEATURES[index].albums.length; i++) {
        var addThis = i == 0 ? ' class="active" aria-current="true"' : '';
        indicators += '<button type="button" data-bs-target="#carouselFX" data-bs-slide-to="' + i + '" ' + addThis + ' aria-label="Slide ' + (i + 1) + '"></button>'
        images += '<div class="carousel-item ' + (i === 0 ? 'active' : '') + '"><img src="' + FEATURES[index].albums[i] + '" class="d-block w-100" alt="..."></div>'
    }
    $('#FEATURE_MODAL_TITLE').html(FEATURES[index].name);
    $('#FEATURE_MODAL_INDICATORS').html(indicators);
    $('#FEATURE_MODAL_INNER').html(images);

    var myModalEl = document.getElementById('featureXImagesModal')
    var featureXImagesModal = new bootstrap.Modal(myModalEl, {});
    featureXImagesModal.show();
    myModalEl.addEventListener('hidden.bs.modal', function(event) {
        $('#FEATURE_MODAL_TITLE').html("");
        $('#FEATURE_MODAL_INDICATORS').html("");
        $('#FEATURE_MODAL_INNER').html("");
    });
}

// Animation
new WOW().init({
    mobile: false
});

// Carousel
$(document).ready(function() {
    $(".owl-carousel").owlCarousel({
        ssr: true,
        infinite: false,
        autoPlay: true,
        autoPlaySpeed: 3000,
        keyBoardControl: true,
        margin: 32,
        responsiveClass: true,
        nav: true,
        responsive: {
            0: {
                breakpoint: { max: 464, min: 0 },
                items: 1
            },
            768: {
                breakpoint: { max: 1024, min: 464 },
                items: 2
            },
            1200: {
                breakpoint: { max: 3000, min: 1024 },
                items: 3
            },
            2000: {
                breakpoint: { max: 4000, min: 3000 },
                items: 5
            },
        }
    });
});

// Tooltip
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
})

// Chart
var canvas_border_color = '#231a37';
var pie_color = '#36256b';
var pie_hover_color = '#ff3549';
var data_set = {
    labels: ['Ecosystem', 'Team & Advisors', 'Liquidity & Marketing', 'Treasury', 'Private sale', 'Public Round', 'Play To Earn'],
    titles: '',
    datasets: [{
        label: "949",
        lineTension: 0,
        backgroundColor: ['#7228e2', pie_color, pie_color, pie_color, pie_color, pie_color, pie_color],
        hoverBackgroundColor: [pie_hover_color, pie_hover_color, pie_hover_color, pie_hover_color, pie_hover_color, pie_hover_color, pie_hover_color],
        borderColor: canvas_border_color,
        borderWidth: 2,
        hoverBorderColor: canvas_border_color,
        data: [25, 20, 10.5, 15, 12, 2.5, 15],
        animationDuration: 400,
    }]
};
var options_set = {
    legend: {
        display: false
    },
    plugins: {
        legend: {
            display: false
        },
        cutoutPercentage: 0,
        maintainAspectRatio: false,
        tooltip: {
            callbacks: {
                title: function(tooltipItem) {
                    return [tooltipItem[0]['label']];
                },
                label: function(tooltipItem, data) {
                    return tooltipItem['raw'] + ' %';
                }
            },
            backgroundColor: 'transparent',
            titleFontSize: 11,
            bodyFontColor: '#fff',
            bodyFontSize: 14,
            bodyFontStyle: 'bold',
            bodySpacing: 0,
            yPadding: 0,
            xPadding: 0,
            yAlign: 'center',
            xAlign: 'center',
            footerMarginTop: 5,
            displayColors: false
        },
    },

    onHover: function(e, i) {
        if (i.length) {
            var _cur_idx = i[0].index + 1;
            $('#tq-chart-legends li').removeClass('active');
            $('#tq-chart-legends li:nth-child(' + _cur_idx + ')').addClass('active');
        } else {
            $('#tq-chart-legends li').removeClass('active');
        }
    }
};
var doughnut_chart = new Chart(
    document.getElementById('tokenChart'), {
        type: 'doughnut',
        data: data_set,
        options: options_set
    }
);
$(window).on('resize', function() {
    doughnut_chart.resize();
});
// var particlesConfig = {
//     particles: {
//         number: {
//             value: 28
//         },
//         color: {
//             value: ["#0182cc", "#00befa", "#0182cc"]
//         },
//         shape: {
//             type: "circle"
//         },
//         opacity: {
//             value: 1,
//             random: !1,
//             anim: {
//                 enable: !1
//             }
//         },
//         size: {
//             value: 3,
//             random: !0,
//             anim: {
//                 enable: !1
//             }
//         },
//         line_linked: {
//             enable: !1
//         },
//         move: {
//             enable: !0,
//             speed: 2,
//             direction: "none",
//             random: !0,
//             straight: !1,
//             out_mode: "out"
//         }
//     },
//     interactivity: {
//         detect_on: "canvas",
//         events: {
//             onhover: {
//                 enable: !1
//             },
//             onclick: {
//                 enable: !1
//             },
//             resize: !0
//         }
//     },
//     retina_detect: !0
// }
// particlesJS('particles-js-1', particlesConfig);
// particlesJS('particles-js-2', particlesConfig);
// particlesJS('particles-js-3', particlesConfig);
// particlesJS('particles-js-4', particlesConfig);
// particlesJS('particles-js-5', particlesConfig);
// particlesJS('particles-js-6', particlesConfig);
window.onload = function() {
    var scrollSpy = new bootstrap.ScrollSpy(document.body, {
        target: '#NAVBAR_HOME'
    })
}